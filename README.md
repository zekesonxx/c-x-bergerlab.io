# How it's Made
This repo contains the source code to [my website.](https://c-x-berger.gitlab.io/)

I got tired of manually writing HTML, but didn't want to make a Jekyll theme to keep my
precious style intact. So I wrote [my own generator](https://gitlab.com/c-x-berger/blagger)
which I've configured here to dump into `public` (the contents of which are
subsequently published by GitLab.)

# Legal Notice
The source code contained within, **and the resulting output on
https://c-x-berger.gitlab.io** is made available under the Creative Commons Attribution
International License, unless otherwise noted.
