title="Assorted Notes on the Raspberry Pi's Boot Partition For Personal Reference and Records-Keeping"
subtitle="you can skip this one"
tags=['raspberry pi', 'linux', 'bootloader', 'arm', 'cross compiler']
::===::
Most of this comes from the [official docs][boot-dir], which details what the heck is
going on in the boot partition of the SD card.

The very short version is that there's a bunch of proprietary Broadcom nonsense, device
tree blobs, and the kernel proper.

This should all be in the first partition of the SD card, which must be a FAT filesystem.
The docs don't mention any size or position requirements beyond "it should be first".

This post assumes you use [the Raspberry Pi kernel fork][rpi-kernel], which contains
(among other things) default configuration files for various Raspberry Pi boards. I'm also
going to focus on the Raspberry Pi 4 and 400, since that's what I have lying around and am
reading all of this stuff for in the first place.

## the kernel

see [this table][kernel-files] for expected filenames.

note that the bootloader can override this using [`os_prefix`] and [`kernel`] in
`config.txt`. The loader will attempt to boot `${os_prefix}${kernel}` (for instance, if
`os_prefix` is `lfs/` the loader will look for `lfs/kernel7l.img` on a raspberry pi 4).

Also note that throughout this post, `CROSS_COMPILE` is set to a value that might not
actually work, depending on how you got your cross-compiler. It should be set to whatever
you need to stick in front of `gcc` to cross-compile for the architecture in question. So
it's not terribly uncommon for `CROSS_COMPILE` to be `aarch64-unknown-linux-gnu-` or even
[`aarch64-rpi4-linux-gnu-`.][ct-ng-rpi4]

### 32 bits

On the Pi 4 (and 400 I assume), `kernel7l.img` is used by default. It should be a
**32-bit** kernel. The `l` denotes the Large Physical Address Extensions, which in short
means [a bigger page table, 40-bit physical address space.][lpae] This is *not* the same
`l` that appears in `armv7l` from `uname -m`, which apparently means the CPU is little
endian. Confused? Good. (On older models, this extension is not implemented and
`kernel7.img` is the default. This also means [different kernel configs.][kernel-configs])

Configure the kernel with `make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-
bcm2711_defconfig`. To build, `make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage
modules dtbs`. (A `zImage` is much like an x86 `bzImage`.) The actual kernel image will be
created as `arch/arm/boot/zImage`.

Some features are built as kernel modules instead of being "baked in". These can be
installed by mounting the root filesystem (i.e. not the boot partition) to `mnt/ext4` and
running `sudo env PATH=$PATH make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-
INSTALL_MOD_PATH=mnt/ext4 modules_install`. (Obviously the mount point is flexible.)

Finally, Kbuild will also build the device tree blobs for you - essentially descriptions
of how hardware is laid out. There are essentially two kinds of these - what might be
called "base" files and ["overlays".][overlays] Overlays alter the basic device tree to
support optional hardware (usually HATs). All of these blobs (overlays and the "base"
trees) need to be copied over to the boot partition as well.

Notably, the kernel.org version of Linux doesn't appear to include _any_ overlays. As a
result, you basically **must** use Raspberry Pi Trading's fork to get support for HATs and
things. That or I'm completely missing something.

```bash
# assume that the boot partition is mounted to `mnt/fat32`
sudo cp arch/arm/boot/dts/*.dtb mnt/fat32/
sudo cp arch/arm/boot/dts/overlays/*.dtb* mnt/fat32/overlays/
sudo cp arch/arm/boot/dts/overlays/README mnt/fat32/overlays/
```

### 64 bits

If `arm_64bit` is set to anything other than 0 in `config.txt`, the system starts in
64-bit mode. This is only available on the Pi 3, 4 and 400. (It's also available for
Rapberry Pi 2B rev 1.2 boards, but not all Pi2Bs are rev 1.2.) `kernel8.img` is used by
default.

Something that might seem odd coming from x86 is that the kernel image may simply be a
gzip archive of a kernel image. Not a self-extracting boot executable, just the direct
result of running `gzip` on the original image file. The bootloader can evidently tell
them apart and decompress as needed. This pattern is also followed by Itanium and RISC-V,
but it seems to basically come down to what happens to be standard on any given
architecture.

Configuration is `make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- bcm2711_defconfig`,
build with `make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- Image modules dtbs`. Note
that here we simply build `Image` and not `zImage` because of the aforementioned
compression shenanigans. The actual kernel image will be created as
`arch/arm64/boot/Image`.

Like with the 32-bit kernel, we probably want to install kernel modules with `make
ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu INSTALL_MOD_PATH=mnt/ext4 modules_install`.

And device tree blobs again:

```bash
# same assumption as before
sudo cp arch/arm64/boot/dts/broadcom/*.dtb mnt/fat32/
sudo cp arch/arm64/boot/dts/overlays/*.dtb* mnt/fat32/overlays/
sudo cp arch/arm64/boot/dts/overlays/README mnt/fat32/overlays/
```

## all the other nonsense

The rest of the boot partition is all [proprietary blobs] loaded by the VideoCore
(i.e. GPU) to continue the boot process after the very very basic setup done by the code
on (in the Pi 4's case) the EEPROM. You just have to [get these from Raspberry
Pi.][firmware]

[boot-dir]: https://www.raspberrypi.com/documentation/computers/configuration.html#the-boot-folder
[rpi-kernel]: https://github.com/raspberrypi/linux
[kernel-files]: https://www.raspberrypi.com/documentation/computers/configuration.html#kernel-files
[`os_prefix`]: https://www.raspberrypi.com/documentation/computers/config_txt.html#os_prefix
[`kernel`]: https://www.raspberrypi.com/documentation/computers/config_txt.html#kernel
[ct-ng-rpi4]: https://github.com/crosstool-ng/crosstool-ng/tree/master/samples/aarch64-rpi4-linux-gnu
[lpae]: https://lwn.net/Articles/444559/
[kernel-configs]: https://www.raspberrypi.com/documentation/computers/linux_kernel.html#32-bit-configs
[overlays]: https://github.com/raspberrypi/linux/tree/rpi-5.10.y/arch/arm/boot/dts/overlays
[proprietary blobs]: https://www.raspberrypi.com/documentation/computers/configuration.html#start-elf-start_x-elf-start_db-elf-start_cd-elf-start4-elf-start4x-elf-start4cd-elf-start4db-elf
[firmware]: https://github.com/raspberrypi/firmware
